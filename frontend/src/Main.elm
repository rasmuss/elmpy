module Main exposing (main)

import Api
import Api.Endpoint as Endpoint
import Browser
import Browser.Navigation as Nav
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (optional, required)
import Json.Encode as Encode
import Task exposing (Task)
import Url exposing (Url)
import Url.Builder exposing (QueryParameter)


type alias Model =
    { mails : List Mail
    , draft : Maybe Mail
    , activeUser : String
    , loggedIn : Bool
    }


type alias Mail =
    { from : String
    , to : String
    , message : String
    }


init : Int -> ( Model, Cmd Msg )
init val =
    ( { mails = []
      , draft = Nothing
      , activeUser = ""
      , loggedIn = False
      }
    , Cmd.none
    )


type Msg
    = Completedfetch (Result Http.Error (List Mail))
    | CompletedSend (Result Http.Error String)
    | FetchMails
    | ComposeMail
    | EnteredUser String
    | EnteredDraftReceiver String
    | EnteredDraftMessage String
    | CloseModal
    | SendMail Mail


validateDraft : Mail -> Bool
validateDraft mail =
    let
        notEmpty str =
            not <| String.isEmpty str
    in
    notEmpty mail.from && notEmpty mail.to && notEmpty mail.message


fetch : String -> Http.Request (List Mail)
fetch user =
    let
        decoder =
            Decode.field "_items" (Decode.list mailDecoder)

        maxResultParam =
            Url.Builder.int "max_results" 50

        whereQuery =
            Encode.object [ ( "receiver", Encode.string user ) ]
                |> Encode.encode 0
                |> Url.Builder.string "where"
    in
    Api.get (Endpoint.messages [ whereQuery, maxResultParam ]) decoder


sendMail : Mail -> Http.Request String
sendMail mail =
    let
        body =
            Encode.object
                [ ( "sender", Encode.string mail.from )
                , ( "receiver", Encode.string mail.to )
                , ( "text", Encode.string mail.message )
                ]
                |> Http.jsonBody
    in
    Api.post (Endpoint.messages []) body (Decode.field "_status" Decode.string)


mailDecoder : Decoder Mail
mailDecoder =
    Decode.succeed Mail
        |> required "sender" Decode.string
        |> required "receiver" Decode.string
        |> required "text" Decode.string


update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        ComposeMail ->
            let
                draft =
                    { from = model.activeUser
                    , to = ""
                    , message = ""
                    }
            in
            ( { model | draft = Just draft }, Cmd.none )

        CloseModal ->
            ( { model | draft = Nothing }, Cmd.none )

        EnteredUser s ->
            ( { model | activeUser = s, loggedIn = False }, Cmd.none )

        EnteredDraftReceiver s ->
            case model.draft of
                Just draft ->
                    ( { model | draft = Just { draft | to = s } }, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        EnteredDraftMessage s ->
            case model.draft of
                Just draft ->
                    ( { model | draft = Just { draft | message = s } }, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        FetchMails ->
            ( model, fetch model.activeUser |> Http.send Completedfetch )

        SendMail mail ->
            ( model, sendMail mail |> Http.send CompletedSend )

        CompletedSend _ ->
            ( { model | draft = Nothing }, Cmd.none )

        {--
        Using result of the fetch to verify if the user is valid or not.
        --}
        Completedfetch (Ok mails) ->
            ( { model | mails = mails, loggedIn = True }, Cmd.none )

        Completedfetch (Err err) ->
            ( { model | loggedIn = False }, Cmd.none )


view : Model -> Html Msg
view model =
    let
        modal =
            case model.draft of
                Just draft ->
                    viewComposeMailModal draft

                Nothing ->
                    text ""

        newMailButton =
            if model.loggedIn then
                button [ class "button", onClick ComposeMail ] [ text "New Mail" ]

            else
                button [ class "button button--disabled" ] [ text "New Mail" ]

        loginButton =
            if String.isEmpty model.activeUser then
                button [ class "button button--disabled" ] [ text "Login" ]

            else
                button [ class "button", onClick FetchMails ] [ text "Login" ]
    in
    div [ class "layout" ]
        [ div [ class "topbar" ]
            [ newMailButton
            , div [ class "topbar__subsection" ]
                [ input [ value model.activeUser, placeholder "User", onInput EnteredUser ] []
                , loginButton
                ]
            ]
        , div [ class "content" ]
            (modal :: List.map viewMailListItem model.mails)
        ]


viewMailListItem : Mail -> Html Msg
viewMailListItem mail =
    div [ class "mail-item" ]
        [ div [ class "mail-item__info-text" ]
            [ text <| "From: " ++ mail.from ]
        , div [ class "mail-item__info-text" ]
            [ text <| "To: " ++ mail.to ]
        , div [ class "mail-item__message" ]
            [ text <| mail.message ]
        ]


viewComposeMailModal : Mail -> Html Msg
viewComposeMailModal draft =
    let
        sendButton =
            if draft |> validateDraft then
                button [ class "button", onClick <| SendMail draft ] [ text "Send" ]

            else
                button [ class "button button--disabled" ] [ text "Send" ]
    in
    div [ class "modal-container" ]
        [ div [ class "modal-background", onClick CloseModal ] []
        , div [ class "modal" ]
            [ div [ class "modal-header" ]
                [ text "Compose Mail"
                , div []
                    [ text "To: "
                    , input [ placeholder "User", value draft.to, onInput EnteredDraftReceiver ] []
                    ]
                ]
            , div [ class "modal-body" ]
                [ textarea [ placeholder "Message", value draft.message, onInput EnteredDraftMessage ] [] ]
            , div [ class "modal-footer" ]
                [ button [ class "button", onClick CloseModal ] [ text "Cancel" ]
                , sendButton
                ]
            ]
        ]


main : Program Int Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view =
            \m ->
                { title = "Mail"
                , body = [ view m ]
                }
        , subscriptions = \_ -> Sub.none
        }
