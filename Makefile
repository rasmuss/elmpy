node_modules = ./frontend/node_modules

# start all containers
.PHONY: up
up:
	docker-compose up --detach

# start all containers in foreground
.PHONY: up-fg
up-fg:
	docker-compose up --abort-on-container-exit

# start backend containers
.PHONY: up-backend
up-backend:
	docker-compose up --detach backend

$(node_modules):
	(cd ./frontend; npm install)

# start backend and webpack dev-server
.PHONY: dev
dev: $(node_modules) up-backend
	(cd ./frontend; npm run dev-debug)

# stop and remove all containers
.PHONY: down
down:
	docker-compose down

# stop all containers
.PHONY: stop
stop:
	docker-compose stop
