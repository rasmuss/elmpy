import os
import json
from eve import Eve

app = Eve()
port = int(os.environ.get('PORT'))

def add_delete_filters(resource, request, lookup):
    if 'where' in request.args:
        conditions = request.args.getlist('where')
        for cond_str in conditions:
            cond = json.loads(cond_str)
            for attrib in cond:
                lookup[attrib] = cond[attrib]

app.on_pre_DELETE += add_delete_filters

if __name__ == '__main__':
    app.run('0.0.0.0', port)
