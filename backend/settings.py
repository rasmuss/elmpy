MONGO_URI = 'mongodb://db'
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'DELETE']

messages_schema = {
        'text': {
            'type': 'string',
            'minlength': 1,
            'required': True
            },
        'receiver': {
            'type': 'string',
            'minlength': 1,
            'required': True
            },
        'sender': {
            'type': 'string',
            'minlength': 1,
            'required': True
            }

}
DOMAIN = { 'messages': { 'schema': messages_schema } }
