# Elm / Python

### Setup
Everything is setup running in Docker containers managed by `docker-compose` ([link](https://docs.docker.com/compose/install/)).

1. Build/Start the everything with `make up` and connect on <http://localhost:3001>.
2. For development - Just run `make dev` and connect on <http://localhost:3000>.
3. Stop running containers with `make stop`.
